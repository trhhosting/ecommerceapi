package cmd

import (
	"ecommerceAPI/scripts"
	"github.com/spf13/cobra"
)

// startCmd represents the start command
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "Start the server",
	Long:  `Start API server`,
	Run: func(cmd *cobra.Command, args []string) {
		scripts.StartServer()
	},
}

func init() {
	rootCmd.AddCommand(startCmd)

}
