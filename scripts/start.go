package scripts

import "ecommerceAPI/lib/src/echoserver"

func StartServer() (err error) {
	var x = echoserver.TLMSMSServer{
		IPAddress: "0.0.0.0",
		Port:      "8090",
	}
	x.TLMServer()
	return
}
