/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import "ecommerceAPI/cmd"

func main() {
	cmd.Execute()
}
