package matterhornwholesale

import (
	"fmt"
	"os"
)

// checking of errors
func checkError(err error) {
	if err != nil {
		fmt.Println("Fatal error ", err.Error())
		os.Exit(1)
	}
}
