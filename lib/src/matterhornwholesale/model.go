package matterhornwholesale

import "encoding/xml"

type Products struct {
	XMLName xml.Name `xml:"products"`
	Text    string   `xml:",chardata"`
	Product []struct {
		Text         string `xml:",chardata"`
		ID           string `xml:"id,attr"`
		Name         string `xml:"name"`
		CreationDate string `xml:"creation_date"`
		Brand        string `xml:"brand"`
		CategoryPath string `xml:"category_path"`
		Category     struct {
			Text string `xml:",chardata"`
			ID   string `xml:"id,attr"`
		} `xml:"category"`
		Color  string `xml:"color"`
		Type   string `xml:"type"`
		Images struct {
			Text     string   `xml:",chardata"`
			ImageURL []string `xml:"image_url"`
		} `xml:"images"`
		Price       string `xml:"price"`
		Description string `xml:"description"`
		Options     struct {
			Text   string `xml:",chardata"`
			Option []struct {
				Text       string `xml:",chardata"`
				ID         string `xml:"id,attr"`
				OptionName string `xml:"option_name"`
				STOCK      string `xml:"STOCK"`
				AvaibleIn  string `xml:"avaible_in"`
				Ean        string `xml:"ean"`
			} `xml:"option"`
		} `xml:"options"`
	} `xml:"product"`
}
type ProductList struct {
	Products struct {
		Product []Product `json:"products"`
	}
}

type Product struct {
	ID       string `json:"_id"`
	Brand    string `json:"brand"`
	Category struct {
		Cdata string `json:"__cdata"`
		ID    string `json:"_id"`
	} `json:"category"`
	CategoryPath string `json:"category_path"`
	Color        string `json:"color"`
	CreationDate string `json:"creation_date"`
	Description  string `json:"description"`
	Images       struct {
		ImageURL []string `json:"image_url"`
	} `json:"images"`
	Name    string `json:"name"`
	Options struct {
		Option []Option `json:"option"`
	} `json:"options"`
	Price string `json:"price"`
	Type  string `json:"type"`
}
type Option struct {
	Stock      string `json:"STOCK"`
	ID         string `json:"_id"`
	AvaibleIn  string `json:"avaible_in"`
	Ean        string `json:"ean"`
	OptionName string `json:"option_name"`
}
