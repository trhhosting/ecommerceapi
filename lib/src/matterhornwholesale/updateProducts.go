package matterhornwholesale

import (
	"ecommerceAPI/lib/src/rest"
	"encoding/xml"
	"fmt"
	"github.com/Jeffail/gabs/v2"
	"io/ioutil"
)

func UpdateProducts() (err error) {
	//download a file from the server
	dat, err := rest.Matterhorn{}.Get()
	if err != nil {
		fmt.Println(err)
		return err
	}
	var products Products
	err = xml.Unmarshal(dat, &products)
	checkError(err)
	var p []Product
	for _, v := range products.Product {
		var options []Option
		for _, s := range v.Options.Option {
			options = append(options, Option{
				Stock:      s.STOCK,
				ID:         s.ID,
				AvaibleIn:  s.AvaibleIn,
				Ean:        s.Ean,
				OptionName: s.OptionName,
			})
		}
		var pD = Product{
			ID:    v.ID,
			Brand: v.Brand,
			Category: struct {
				Cdata string `json:"__cdata"`
				ID    string `json:"_id"`
			}{Cdata: v.Category.Text, ID: v.Category.ID},
			CategoryPath: v.CategoryPath,
			Color:        v.Color,
			CreationDate: v.CreationDate,
			Description:  v.Description,
			Images: struct {
				ImageURL []string `json:"image_url"`
			}{ImageURL: v.Images.ImageURL},
			Name: v.Name,
			Options: struct {
				Option []Option `json:"option"`
			}{Option: options},
			Price: v.Price,
			Type:  v.Type,
		}
		p = append(p, pD)
	}
	//fmt.Println(p)
	//dat, err := json.Marshal(p)
	gabs.Wrap(p).Bytes()
	err = ioutil.WriteFile("data/product.json", gabs.Wrap(p).BytesIndent("", "\t"), 0644)
	checkError(err)
	return ioutil.WriteFile("data/matterhorn_products.xml", dat, 0644)
}
