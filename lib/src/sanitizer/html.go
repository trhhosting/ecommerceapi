package sanitizer

import (
	"github.com/microcosm-cc/bluemonday"
	"strings"
)

func CleanHtmlString(html string) string {
	// Do this once for each unique policy, and use the policy for the life of the program
	// Policy creation/editing is not safe to use in multiple goroutines
	p := bluemonday.StrictPolicy()
	//p := bluemonday.UGCPolicy()
	p.AllowElements("h", "img")
	// The policy can then be used to sanitize lots of input and it is safe to use the policy in multiple goroutines
	html = strings.ReplaceAll(html, `&nbsp;`, " ")
	htmlString := p.Sanitize(html)
	return htmlString
}
func CleanHtmlTemplate(html string) []byte {
	// Do this once for each unique policy, and use the policy for the life of the program
	// Policy creation/editing is not safe to use in multiple goroutines
	p := bluemonday.StrictPolicy()
	//p := bluemonday.UGCPolicy()
	p.AllowElements("h", "br")
	// The policy can then be used to sanitize lots of input and it is safe to use the policy in multiple goroutines
	html = strings.ReplaceAll(html, `&nbsp;`, " ")
	htmlString := p.Sanitize(html)
	return []byte(htmlString)
}
