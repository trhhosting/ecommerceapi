package sanitizer

import (
	"fmt"
	"testing"
)

func TestCleanHtmlString(t *testing.T) {
	dat := CleanHtmlString(htmlData)
	fmt.Println(dat)
}

var htmlData = `      <section class="footer-style-2 mt-50 pt-50 pb-100">
        <div class="container">
          <div class="footer-top">
            <div class="footer-top-left mt-50">
              <a href="">
                <img src="/static/icons/favicon.svg" height="100vh" alt="" />
              </a>
            </div>
            <div class="footer-top-right mt-50">
              <span class="title">We Accept:</span>
              <img src="assets/images/payment.png" alt="" />
            </div>
          </div>
  
          <div class="footer-widget-wrapper pt-20">
            <div class="row justify-content-between">
              <div class="col-xl-7 col-lg-8">
                <div class="footer-link-widget">
                  <div class="footer-link">
                    <h5 class="footer-title">My Account</h5>
  
                    <ul class="link">
                      <li><a href="">Orders</a></li>
                      <li><a href="">Downloads</a></li>
                      <li><a href="">Addresses</a></li>
                      <li><a href="">Account details</a></li>
                      <li><a href="">Logout</a></li>
                    </ul>
                  </div>
                  <div class="footer-link">
                    <h5 class="footer-title">Information</h5>
  
                    <ul class="link">
                      <li><a href="">About Us</a></li>
                      <li><a href="">Contact Us</a></li>
                      <li><a href="">Downloads</a></li>
                      <li><a href="">Sitemap</a></li>
                      <li><a href="">FAQs Page</a></li>
                      <li><a href="">Comming Soon</a></li>
                      <li><a href="">404 Page</a></li>
                    </ul>
                  </div>
                  <div class="footer-link">
                    <h5 class="footer-title">Categories</h5>
  
                    <ul class="link">
                      <li><a href="">Outfit</a></li>
                      <li><a href="">Sunglassess</a></li>
                      <li><a href="">Watches</a></li>
                      <li><a href="">Bags</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="footer-widget-subscribe">
                  <h5 class="footer-title">PRODUCT</h5>
                  <p>
                    Be the first to know when new products drop and get
                    behind-the-scenes content straight.
                  </p>
  
                  <div class="subscribe-form">
                    <form action="#">
                      <div class="single-form form-default">
                        <label>Enter your email address</label>
                        <div class="form-input">
                          <input type="text" placeholder="user@email.com" />
                          <i class="mdi mdi-account"></i>
                          <button class="main-btn primary-btn">
                            <span class="mdi mdi-send"></span>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
  
          <div class="footer-bottom">
            <div class="footer-copyright">
              <p>Powered by &copy; <a href="https://github.com/trhhosting">TRH Hosting</a>,  All rights reserved.
              </p>
            </div>
            <div class="footer-follow">
              <span class="title">Follow Us:</span>
              <ul class="social-follow">
                <li>
                  <a href=""><i class="lni lni-facebook-filled"></i></a>
                </li>
                <li>
                  <a href=""><i class="lni lni-twitter-filled"></i></a>
                </li>
                <li>
                  <a href=""><i class="lni lni-linkedin-original"></i></a>
                </li>
                <li>
                  <a href=""><i class="lni lni-instagram-original"></i></a>
                </li>
                <li>
                  <a href=""><i class="lni lni-whatsapp"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>`
