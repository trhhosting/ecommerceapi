package getTerms

import (
	"encoding/base64"
	"fmt"
	"github.com/rakyll/statik/fs"
	"io/ioutil"
	"testing"

	_ "ecommerceAPI/lib/src/statik" // TODO: Replace with the absolute import path
)

func TestPolicyFormat(t *testing.T) {
	//dat, err := ioutil.ReadFile("rawData/donotsell.html")
	//if err != nil {
	//	t.Name()
	//	fmt.Println(err)
	//	t.Fail()
	//}
	statikFS, err := fs.New()
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	r, err := statikFS.Open("/html/donotsell.html")
	if err != nil {
		fmt.Println(err)
	}
	defer r.Close()
	contents, err := ioutil.ReadAll(r)
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	fmt.Println(contents)
	base := policyFormat(contents, "Txt Me", "txtsme.com")
	fmt.Println(base)
	htm, err := base64.URLEncoding.DecodeString(base)
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	fmt.Println(string(htm))
}
func TestPolicyGenerator(t *testing.T) {
	dat, err := PolicyGenerator("donotsell", Company{
		Name:   "The World Is one",
		Domain: "theworld.com",
	})
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	//fmt.Println(dat)
	htm, _ := base64.URLEncoding.DecodeString(dat)
	fmt.Println(string(htm))
}
