package getTerms

import (
	"ecommerceAPI/lib/src/sanitizer"
	_ "ecommerceAPI/lib/src/statik"
	"encoding/base64"
	"fmt"
	"github.com/rakyll/statik/fs"
	"io/ioutil"
	"regexp"
)

func policyFormat(input []byte, companyName, companySite string) (output string) {
	//&insert_company_name&
	//&insert_company_website&
	regCompanyName := regexp.MustCompile(`(&insert_company_name&)`)
	regCompanySite := regexp.MustCompile(`(&insert_company_website&)`)

	tmp := regCompanyName.ReplaceAll(input, []byte(companyName))
	tmp = regCompanySite.ReplaceAll(tmp, []byte(companySite))
	send := sanitizer.CleanHtmlTemplate(string(tmp))
	return base64.URLEncoding.EncodeToString(send)
}

type Company struct {
	Name   string `json:"name"`
	Domain string `json:"domain"`
}

func PolicyGenerator(policyFilename string, company Company) (output string, err error) {
	statikFS, err := fs.New()
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	r, err := statikFS.Open(fmt.Sprintf("/html/%v.html", policyFilename))
	if err != nil {
		fmt.Println(err)
		return "", err

	}
	defer r.Close()
	contents, _ := ioutil.ReadAll(r)
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	base := policyFormat(contents, company.Name, company.Name)

	return base, err
}
