package configure

type ApiConfig struct {
	GoogleAPI     []string `json:"google_api"`
	RapidAPI      []string `json:"rapid_api"`
	UserCert  string   `json:"proxy_user_cert"`
	UserKey   string   `json:"proxy_user_key"`
	TelegramUsers []struct {
		AppName  string `json:"app_name"`
		BotName  string `json:"bot_name"`
		BotToken string `json:"bot_token"`
		ChatID   int64  `json:"chat_id"`
	} `json:"telegram_users"`
	Tlmconfig struct {
		Adapter     string `json:"adapter"`
		Host        string `json:"host"`
		Name        string `json:"name"`
		Password    string `json:"password"`
		Port        string `json:"port"`
		Root        string `json:"root"`
		RootPassword string `json:"root_password"`
		Rootcert    string `json:"rootcert"`
		Rootkey     string `json:"rootkey"`
		Sslcert     string `json:"sslcert"`
		Sslkey      string `json:"sslkey"`
		Sslmode     string `json:"sslmode"`
		Sslrootcert string `json:"sslrootcert"`
		User        string `json:"user"`
	} `json:"tlmconfig"`
	KaggleApi struct {
		Username string `json:"username"`
		Key      string `json:"key"`
	} `json:"kaggle_api"`
}
type Auth struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}
type GoAuth Auth
type ProxyApiAuth Auth 