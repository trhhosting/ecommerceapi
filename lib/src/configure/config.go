package configure

import (
	"os"
	"fmt"
	"regexp"
	"io/ioutil"
	"encoding/json"
	"path/filepath"
	"encoding/base64"
)



func SetupConfig()  (conf ApiConfig,  err error)  {

	var confName = "config.json"
	checkConfig(confName, []byte(dconfig))
	confName = filepath.Join(".config", confName)
	dat, err := ioutil.ReadFile(confName)
	if err != nil {
		return ApiConfig{}, err
	}
	json.Unmarshal(dat, &conf)

	confName = "telegram_user_apps.json"
	dat, err = json.Marshal(conf.TelegramUsers)
	saveConfig(confName, dat)

	confName = "telegram_user_apps.json"
	dat, err = json.Marshal(conf.TelegramUsers)
	saveConfig(confName, dat)

	confName = "google_api.json"
	googleApi := map[string][]string{"google_api": conf.GoogleAPI}
	dat, err = json.Marshal(googleApi)
	saveConfig(confName, dat)


	confName = "rapid_api.json"
	rapidApi := map[string][]string{"rapid_api": conf.RapidAPI}
	dat, err = json.Marshal(rapidApi)
	saveConfig(confName, dat)

	confName = "kaggle_api.json"
	kaggleApi := map[string]struct{
		Username string `json:"username"`
		Key string `json:"key"`
	}{"kaggle_api": conf.KaggleApi}
	dat, err = json.Marshal(kaggleApi)
	saveConfig(confName, dat)

	//Cockroach settings
	confName = "tlmconfig.json"
	dat, err = json.Marshal(conf.Tlmconfig)
	saveConfig(confName, dat)
	renderCaCert()
	return
}

func checkConfig(filename string, data []byte)  {
	os.MkdirAll(".config", 0744)
	pth := filepath.Join(".config", filename)
	reg := regexp.MustCompile("The system cannot find the file specified")
	reg2 := regexp.MustCompile("no such file or directory")
	file, err := os.Open(pth)
	if err != nil {
		if reg.MatchString(err.Error()) || reg2.MatchString(err.Error()){
			fmt.Println("creating default " + filename + " file")
			ioutil.WriteFile(pth, data, 0644)
			return
		}
	}
	file.Close()
	//ioutil.WriteFile(pth, data, 0644)
}

func saveConfig(filename string, data []byte)  {
	pth := filepath.Join(".config", filename)
	ioutil.WriteFile(pth, data, 0644)
}

func renderCerts(data string, certname string)  ( err error) {
	pth := filepath.Join(".config","cert")
	os.MkdirAll(pth, 0655)
	pth = filepath.Join(pth, certname)
	sDec, _ := base64.StdEncoding.DecodeString(data)
	err = ioutil.WriteFile(pth, []byte(sDec), 0400)
	return
}

func renderCaCert()  ( err error) {
	pth := filepath.Join(".config","cert")
	os.MkdirAll(pth, 0655)
	pth = filepath.Join(pth, "ca.crt")
	return ioutil.WriteFile(pth, []byte(certCa), 0400)
}

func Encoder()  (usrCert, usrKey string) {
	usrKey = base64.StdEncoding.EncodeToString([]byte(userKey))
	usrCert = base64.StdEncoding.EncodeToString([]byte(userCert))
 return
}