package configure

import (
	"testing"
	"fmt"
	"path/filepath"
	"io/ioutil"
	"encoding/json"
)

func TestGetConfig(t *testing.T) {
	dat, err := SetupConfig()
	if err != nil {
	   fmt.Println(err)
	}
	fmt.Println(dat)
}
func TestRenderCerts(t *testing.T) {
	var confName = "config.json"
	checkConfig(confName, []byte(dconfig))
	confName = filepath.Join(".config", confName)
	dat, err := ioutil.ReadFile(confName)
	if err != nil {
		t.Name()
		t.Fail()
	}
	var conf ApiConfig
	json.Unmarshal(dat, &conf)

	renderCerts(conf.UserCert, "")

}
func TestEncoder(t *testing.T) {
	cert, key := Encoder()
	fmt.Println(cert)
	fmt.Println(key)
}
