package cjdropshipping

import (
	"encoding/json"
	"fmt"
)

func ListCategories(data []byte) (categoriesList []string, err error) {
	var categories Categories
	err = json.Unmarshal(data, &categories)
	if err != nil {
		return nil, err
	}
	for _, category := range categories.Data {
		categoriesList = append(categoriesList, fmt.Sprintf("%v: %v", category.ID, category.Name))
		for _, subset := range category.SubsetJSON {
			categoriesList = append(categoriesList, fmt.Sprintf("%v: %v > %v", subset.ID, category.Name, subset.Name))
			for _, subset2 := range subset.SubsetJSON {
				categoriesList = append(categoriesList, fmt.Sprintf("%v: %v > %v > %v", subset2.ID, category.Name, subset.Name, subset2.Name))
			}
		}
	}
	return
}
