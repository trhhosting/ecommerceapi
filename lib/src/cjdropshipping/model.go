package cjdropshipping

type Categories struct {
	Code      bool  `json:"code"`
	CountSize int64 `json:"countSize"`
	Data      []struct {
		ID         string `json:"id"`
		Name       string `json:"name"`
		SubsetJSON []struct {
			ID         string `json:"id"`
			Name       string `json:"name"`
			ParentPid  string `json:"parentPid"`
			SubsetJSON []struct {
				ID        string `json:"id"`
				Name      string `json:"name"`
				ParentPid string `json:"parentPid"`
			} `json:"subsetJson"`
		} `json:"subsetJson"`
	} `json:"data"`
	PageNum int64 `json:"pageNum"`
}
type Products struct {
	Code      bool  `json:"code"`
	CountSize int64 `json:"countSize"`
	Data      []struct {
		BigImg        string `json:"bigImg"`
		Category      string `json:"category"`
		CategoryID    string `json:"categoryId"`
		CommodityData []struct {
			Standard                         string   `json:"STANDARD"`
			CommodityDifferenceOption        string   `json:"commodityDifferenceOption"`
			ID                               string   `json:"id"`
			Img                              string   `json:"img"`
			PackWerght                       float32  `json:"packWerght"`
			ParentID                         string   `json:"parentID"`
			Price                            string   `json:"price"`
			Sku                              string   `json:"sku"`
			Specifications                   []string `json:"specifications"`
			VarietyCommodityDifferenceOption []string `json:"varietyCommodityDifferenceOption"`
			Weight                           float32  `json:"weight"`
		} `json:"commodityData"`
		CommodityDifferenceOption []string `json:"commodityDifferenceOption"`
		CommodityProperty         []string `json:"commodityProperty"`
		Description               string   `json:"description"`
		ID                        string   `json:"id"`
		ImgOption                 []string `json:"imgOption"`
		MaterialOption            []string `json:"materialOption"`
		Name                      string   `json:"name"`
		PackingOption             []string `json:"packingOption"`
		Sku                       string   `json:"sku"`
		Unit                      string   `json:"unit"`
	} `json:"data"`
	PageNum int64 `json:"pageNum"`
}
type ProductData struct {
	BigImg        string `json:"bigImg"`
	Category      string `json:"category"`
	CategoryID    string `json:"categoryId"`
	CommodityData []struct {
		Standard                         string   `json:"STANDARD"`
		CommodityDifferenceOption        string   `json:"commodityDifferenceOption"`
		ID                               string   `json:"id"`
		Img                              string   `json:"img"`
		PackWerght                       float32  `json:"packWerght"`
		ParentID                         string   `json:"parentID"`
		Price                            string   `json:"price"`
		Sku                              string   `json:"sku"`
		Specifications                   []string `json:"specifications"`
		VarietyCommodityDifferenceOption []string `json:"varietyCommodityDifferenceOption"`
		Weight                           float32  `json:"weight"`
	} `json:"commodityData"`
	CommodityDifferenceOption []string `json:"commodityDifferenceOption"`
	CommodityProperty         []string `json:"commodityProperty"`
	Description               string   `json:"description"`
	ID                        string   `json:"id"`
	ImgOption                 []string `json:"imgOption"`
	MaterialOption            []string `json:"materialOption"`
	Name                      string   `json:"name"`
	PackingOption             []string `json:"packingOption"`
	Sku                       string   `json:"sku"`
	Unit                      string   `json:"unit"`
}
type Product struct {
	Name            string            `json:"name"`
	ImageList       []string          `json:"imageList"`
	MainImage       string            `json:"mainImage"`
	ProductData     []ProductItemData `json:"productData"`
	ProductProperty []string          `json:"productProperty"`
	Description     string            `json:"description"`
}
type ProductItemData struct {
	Standard                         string   `json:"STANDARD"`
	CommodityDifferenceOption        string   `json:"commodityDifferenceOption"`
	ID                               string   `json:"id"`
	Img                              string   `json:"img"`
	PackWeight                       float32  `json:"packWeight"`
	ParentID                         string   `json:"parentID"`
	Price                            float32  `json:"price"`
	Sku                              string   `json:"sku"`
	Specifications                   []string `json:"specifications"`
	VarietyCommodityDifferenceOption []string `json:"varietyCommodityDifferenceOption"`
	Weight                           float32  `json:"weight"`
}
type ProductCategory struct {
	Name string `json:"name"`
	ID   string `json:"id"`
}
