package cjdropshipping

import (
	"ecommerceAPI/lib/src/rest"
	"ecommerceAPI/lib/src/sanitizer"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/Jeffail/gabs/v2"
	"io/ioutil"
	"os"
	"strings"
)

const apiKey = `63cfb436f89c4464b5fffac188f22fa9`

func GetProducts(categoryID string) (productList []Product, err error) {
	var data []byte
	if _, errr := os.Stat(fmt.Sprintf("products_%v.json", categoryID)); os.IsNotExist(errr) {

		x := rest.CjDropShipping{
			ApiKey: apiKey,
			Uri:    "api/commodity/getCommodity",
			Payload: gabs.Wrap(struct {
				PageNum    int    `json:"pageNum"`
				PageSize   int    `json:"pageSize"`
				CategoryID string `json:"categoryId"`
			}{
				PageNum:    1,
				PageSize:   50,
				CategoryID: categoryID,
			}).Bytes(),
		}
		dat, ex := x.Post()
		if ex != nil {
			return
		}
		data = dat
		_ = ioutil.WriteFile(fmt.Sprintf("products_%v.json", categoryID), data, 0644)
	} else {
		data, err = ProductsCached(categoryID)
		if err != nil {
			return
		}
	}
	var products Products
	err = json.Unmarshal(data, &products)
	if err != nil {
		return
	}
	var sendProducts Products
	for _, i2 := range products.Data {
		//i2.Description = strings.ReplaceAll(i2.Description, "<img", "<img hidden")
		sendProducts.Data = append(sendProducts.Data, i2)
	}
	return formatProducts(gabs.Wrap(sendProducts.Data).Bytes())

}

func formatProducts(products []byte) (productList []Product, err error) {
	var prods []ProductData
	err = json.Unmarshal(products, &prods)
	if err != nil {
		return
	}
	var prod []Product
	for _, product := range prods {
		var pid []ProductItemData
		for _, datum := range product.CommodityData {
			//convert string into int
			var price float32
			_, err = fmt.Sscanf(datum.Price, "%f", &price)
			pid = append(pid, ProductItemData{
				Standard:                         datum.Standard,
				CommodityDifferenceOption:        datum.CommodityDifferenceOption,
				ID:                               datum.ID,
				Img:                              datum.Img,
				PackWeight:                       datum.PackWerght,
				ParentID:                         datum.ParentID,
				Price:                            price,
				Sku:                              datum.Sku,
				Specifications:                   datum.Specifications,
				VarietyCommodityDifferenceOption: datum.VarietyCommodityDifferenceOption,
				Weight:                           datum.Weight,
			})
		}
		var p = Product{
			Name:            product.Name,
			ImageList:       product.ImgOption,
			MainImage:       product.BigImg,
			ProductData:     pid,
			ProductProperty: product.CommodityProperty,
			//Description:     product.Description,
			Description: sanitizer.CleanHtmlString(product.Description),
		}
		//fmt.Println(fmt.Sprintf("%v", product.Name))
		//fmt.Println(fmt.Sprintf("%v", product.ImgOption))
		//fmt.Println(fmt.Sprintf("%v", product.BigImg))
		//fmt.Println(fmt.Sprintf("%v", product.CommodityData))
		//fmt.Println(fmt.Sprintf("%v", product.CommodityProperty))
		prod = append(prod, p)
	}
	for _, product := range prod {
		//encoded := base64.StdEncoding.EncodeToString(gabs.Wrap(product).Bytes())
		//fmt.Println(encoded)
		productList = append(productList, product)
	}
	return
}

func GetCategoryID(categoryName string) (categoryID []ProductCategory, err error) {
	categoryName = strings.ToLower(categoryName) //convert to lowercase
	//Check if file exists
	if _, errr := os.Stat("categories_raw.json"); os.IsNotExist(errr) {
		x := rest.CjDropShipping{
			ApiKey: apiKey,
			Uri:    "api/commodity/getCategory",
			Payload: gabs.Wrap(struct {
				PageNum  int `json:"pageNum"`
				PageSize int `json:"pageSize"`
			}{
				PageNum:  1,
				PageSize: 50,
			}).Bytes(),
		}
		data, er := x.Post()
		var cats Categories
		er = json.Unmarshal(data, &cats)
		if er != nil {
			return categoryID, er
		}
		_ = ioutil.WriteFile("categories_raw.json", data, 0644)
	}

	catList, err := CategoriesCached()
	if err != nil {
		return nil, err
	}
	_ = ioutil.WriteFile("categories.json", gabs.Wrap(catList).Bytes(), 0644)
	//regCat := regexp.MustCompile(`^([0-9]-[0-9])|([0-9][0-9]-[0-9])|([0-9][0-9]-[0-9][0-9]) `)

	for _, category := range catList {
		//fmt.Println(fmt.Sprintf("%v || %v", category.Name, categoryName))
		catName := strings.ToLower(category.Name)
		if catName == categoryName {
			categoryID = append(categoryID, category)
		}
	}
	return
}
func CategoriesCached() (catList []ProductCategory, err error) {
	data, err := ioutil.ReadFile("categories_raw.json")
	var cats Categories
	err = json.Unmarshal(data, &cats)
	if err != nil {
		return
	}
	for ic, cat := range cats.Data {
		for _, s := range cat.SubsetJSON {
			for i, s2 := range s.SubsetJSON {
				catList = append(catList, ProductCategory{
					ID:   s2.ID,
					Name: fmt.Sprintf("%v-%v ", ic, i) + s2.Name,
				})
			}
		}
	}
	return
}
func ProductsCached(categoryID string) (products []byte, err error) {
	return ioutil.ReadFile(fmt.Sprintf("products_%v.json", categoryID))
}

func listBase64Categories() (baseList map[string]string, err error) {
	catList, err := CategoriesCached()
	if err != nil {
		return nil, err
	}
	var base64List = make(map[string]string)
	for _, category := range catList {
		tmp := base64.URLEncoding.EncodeToString(gabs.Wrap(category.Name).Bytes())
		base64List[category.Name] = tmp
	}
	return base64List, nil
}
