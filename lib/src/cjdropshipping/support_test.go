package cjdropshipping

import (
	"fmt"
	"io/ioutil"
	"testing"
)

func TestListCategories(t *testing.T) {
	data, err := ioutil.ReadFile("jsonData/cjCats.json")
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	cats, err := ListCategories(data)
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	var catsString string
	for i, cat := range cats {
		fmt.Println(i, cat)
		catsString += fmt.Sprintf("%v: ", i) + cat + "\n"
	}
	ioutil.WriteFile("results.txt", []byte(catsString), 0644)
}
