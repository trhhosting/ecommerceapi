package cjdropshipping

import (
	"fmt"
	"github.com/Jeffail/gabs/v2"
	"io/ioutil"
	"testing"
)

func TestGetProducts(t *testing.T) {
	productList, err := GetProducts("5F6BBD36-AFDE-4433-81D1-8684781E04DE")
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	fmt.Println(productList)
}

func TestFormatProducts(t *testing.T) {
	dat, err := ioutil.ReadFile("jsonData/products_data.json")
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	productList, err := formatProducts(dat)
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	fmt.Println(productList)
	//for i, encoded := range productList {
	//	fmt.Println(i)
	//	decoded, err := base64.StdEncoding.DecodeString(encoded)
	//	if err != nil {
	//		fmt.Println("decode error:", err)
	//		return
	//	}
	//	fmt.Println(encoded)
	//	fmt.Println(string(decoded))
	//}
}

func TestGetCategoryID(t *testing.T) {
	dat, err := GetCategoryID("6-4 Diamond Painting Cross Stitch")
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	fmt.Println(dat)
}

func TestProductCategory(t *testing.T) {
	var dat ProductCategory
	fmt.Println(gabs.Wrap(dat).String())
}
func TestProduct(t *testing.T) {
	var dat Product
	var dat2 ProductItemData
	fmt.Println(gabs.Wrap(dat).String())
	fmt.Println(gabs.Wrap(dat2).String())
}
func TestCategoriesCached(t *testing.T) {
	dat, err := CategoriesCached()
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	fmt.Println(dat)
}
func TestListBase64Categories(t *testing.T) {
	dat, err := listBase64Categories()
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	fmt.Println(dat)
	_ = ioutil.WriteFile("data/categories_data.json", gabs.Wrap(dat).Bytes(), 0644)
}
