package sms

import "github.com/jinzhu/gorm"

type PhoneDevice struct {
	gorm.Model
	PhoneNumber string `gorm:"size:15;unique",json:"phone_number"`
	Account string

}