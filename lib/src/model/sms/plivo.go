package sms

import "github.com/jinzhu/gorm"

type PlivoSmsRequest struct {
/*
To | [18602220715]
From | [18602666623]
TotalRate | [0]
Units | [1]
Text | [Hello]
TotalAmount | [0]
Type | [sms]
MessageUUID | [1ad857b0-a435-11e8-8824-02299d5658e8]
*/
	gorm.Model
	To string `gorm:"varchar(25);not null",json:"to"`
	From string `gorm:"varchar(25);not null",json:"from"`
	TotalRate string `gorm:"varchar(25);not null",json:"total_rate"`
	Units string `json:"units"`
	Text string `json:"text"`
	TotalAmount string `json:"total_amount"`
	Type string `json:"type"`
	MessageID string `json:"message_id"`
}

