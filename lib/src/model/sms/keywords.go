package sms

import (
	"github.com/jinzhu/gorm"
	"strings"
)

// https://tlm.pw/8ekoReE3EQw5
type KeywordShort struct {
	gorm.Model
	// Table custom fields
	Domain string `gorm:"not null;DEFAULT:'gcx.pw'"`
	ShortUri string `gorm:"size:25",json:"short_uri"`
	FullUri string `gorm:"type:varchar(255);not_null",json:"full_uri"`
}

type KeywordAction struct {
	gorm.Model
	Message string `gorm:"size:110"`
	FaceValue int `json:"face_value;default:0"`
	Salt string `gorm:"size:30"json:"salt"`
}

// https://tlm.pw/123456789012345
//Keyword all keywords will be stored without spaces and lowercase
type Keyword struct {
	gorm.Model
	Keyword string `gorm:"size:50;index;unique"json:"keyword"`
	PhoneNumber string `gorm:"size:11"json:"phone_number"`
	KeywordAction KeywordAction
	KeywordActionID uint
	KeywordShort KeywordShort
	KeywordShortID uint
}

func (k Keyword) Save(action KeywordAction)  (keyword Keyword) {
	kwrd := strings.ReplaceAll(k.Keyword, " ", "" )
	kwrd = strings.ToLower(kwrd)
	keyword = k
	keyword.KeywordAction = action
	keyword.Keyword = kwrd
	return
}
