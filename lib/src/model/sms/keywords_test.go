package sms

import (
	"testing"
	"fmt"
)

func TestKeyword(t *testing.T) {

	var k = Keyword{
		Keyword: "Hello World",
		PhoneNumber: "+18605248123",
	}
	var ka = KeywordAction{
		Message: "test",
		URL: "https://gcx.pw",
		FaceValue: 5000,
		Salt: "NhwypJuK3A",
	}
	x := k.Save(ka)
	fmt.Println(x)
}
