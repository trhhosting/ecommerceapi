package sms

import (
	"github.com/jinzhu/gorm"
)


type SentMsg struct {
	gorm.Model
	Sid               string `gorm:"size:50",json:"sid"`
	To                string `gorm:"size:15",json:"to"`
	From              string `gorm:"size:15",json:"from"`
	Body              string `gorm:"size:160",json:"body"`
	StatusCallbackURL string `gorm:"size:80",json:"status_callback_url"`
}

