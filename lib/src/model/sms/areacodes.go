package sms

import "github.com/jinzhu/gorm"

type NpaCode struct {
	gorm.Model
	AreaCode string `gorm:"size:3;index",json:"area_code"`
	City string `json:"city"`
	State string `gorm:"size:100",json:"state"`
	CountryCode string `gorm:"size:2",json:"country_code"`
	Latitude string `gorm:"size:10",json:"latitude"`
	Longitude string `gorm:"size:10",json:"longitude"`
}

