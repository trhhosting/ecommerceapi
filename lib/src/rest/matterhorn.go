package rest

import (
	"github.com/go-resty/resty/v2"
)

type Matterhorn struct{}

var matterhornBaseUri = "http://srv0.matterhorn-wholesale.com/xmldata/products_full.php?type=products_xml"

func (mh Matterhorn) Get() (rsp []byte, err error) {
	client := resty.New()
	client.SetHeaders(map[string]string{
		"User-Agent": UserAgent,
	})
	resp, err := client.R().
		EnableTrace().
		Get(matterhornBaseUri)

	return resp.Body(), nil
}
