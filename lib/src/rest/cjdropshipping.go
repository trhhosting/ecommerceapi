package rest

import (
	"fmt"
	"github.com/go-resty/resty/v2"
)

type CjDropShipping struct {
	ApiKey  string `json:"api_key"`
	Uri     string `json:"uri"`
	Payload []byte `json:"payload"`
}

const cjdropshippingUrlBase = "https://developers.cjdropshipping.com"

func (cj CjDropShipping) Post() (data []byte, err error) {
	client := resty.New()
	client.SetHeaders(map[string]string{
		"User-Agent":      UserAgent,
		"Content-Type":    "application/json",
		"CJ-Access-Token": cj.ApiKey,
	})
	//client.SetAuthToken(cj.ApiKey)
	uri := fmt.Sprintf("%v/%v", cjdropshippingUrlBase, cj.Uri)
	resp, err := client.R().
		SetBody(cj.Payload).
		Post(uri)

	return resp.Body(), err
}
