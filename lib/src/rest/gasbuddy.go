package rest

import (
	"fmt"
	"github.com/go-resty/resty/v2"
	"net/url"
)

type GasBuddy struct {
	Zipcode   string `json:"zipcode"`
	FuelType  int    `json:"fuel_type"`
	MaxHours  int    `json:"max_hours"`
	PayMethod string `json:"pay_method"`
}

var gasBuddyBaseUri = "https://www.gasbuddy.com/"

func (gb GasBuddy) Get() (rsp []byte, err error) {
	client := resty.New()
	client.SetHeaders(map[string]string{
		"User-Agent": UserAgent,
	})
	params := url.Values{}
	params.Add("search", fmt.Sprintf("%v", gb.Zipcode))
	params.Add("fuel", fmt.Sprintf("%v", gb.FuelType))
	params.Add("maxAge", fmt.Sprintf("%v", gb.MaxHours))
	params.Add("method", fmt.Sprintf("%v", gb.PayMethod))

	uri := fmt.Sprintf("%vhome?%v", gasBuddyBaseUri, params.Encode())
	//fmt.Println(uri)
	//uri := fmt.Sprintf("%vhome?search=%v&fuel=%v&maxAge=%v&method=%v", gasBuddyBaseUri, gb.Zipcode, gb.FuelType, gb.MaxHours, gb.PayMethod)
	resp, err := client.R().
		EnableTrace().
		Get(uri)

	return resp.Body(), nil
}
