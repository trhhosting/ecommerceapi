package rest

import (
	"fmt"
	"github.com/Jeffail/gabs/v2"
	"testing"
)

func TestCjDropShipping_Post(t *testing.T) {
	x := CjDropShipping{
		ApiKey: "63cfb436f89c4464b5fffac188f22fa9",
		Uri:    "api/commodity/getCategory",
		Payload: gabs.Wrap(struct {
			PageNum  int `json:"pageNum"`
			PageSize int `json:"pageSize"`
		}{
			PageNum:  1,
			PageSize: 50,
		}).Bytes(),
	}
	dat, err := x.Post()
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	fmt.Println(string(dat))

}
