package rest

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type GeoLocation struct {
	Address struct {
		Line1    string `json:"line1"`
		Locality string `json:"locality"`
		Region   string `json:"region"`
		Country  string `json:"country"`
	} `json:"address"`
	Lat    string `json:"lat"`
	Lon    string `json:"lon"`
	Lang   string `json:"lang"`
	Limit  int    `json:"limit"`
	Bias   string `json:"bias"`
	Filter string `json:"filter"`
}

var geoLocationBaseUri = "https://verify-and-geocode-address.p.rapidapi.com/v1/geocode/"

func (gl GeoLocation) Get() (rsp []byte, err error) {
	params := url.Values{}
	params.Add("text", fmt.Sprintf("%v,%v,%v,%v", gl.Address.Line1, gl.Address.Locality, gl.Address.Region, gl.Address.Country))
	params.Add("lang", fmt.Sprintf("%v", gl.Lang))
	//params.Add("type", fmt.Sprintf("%v", "street"))
	params.Add("limit", fmt.Sprintf("%v", gl.Limit))
	params.Add("filter", fmt.Sprintf("%v", gl.Filter))
	//params.Add("bias", fmt.Sprintf("%v", gl.Bias))

	//params.Add("maxAge", fmt.Sprintf("%v", gb.MaxHours))
	//params.Add("method", fmt.Sprintf("%v", gb.PayMethod))

	//uri := "https://verify-and-geocode-address.p.rapidapi.com/v1/geocode/search?text=10%20Bd%20du%20Palais%2C%2075001%20Paris%2C%20France&lat=40.74&lon=-73.98&limit=1&lang=en&bias=proximity%3A41.2257145%2C52.971411&filter=countrycode%3Ade%2Ces%2Cfr"
	uri := fmt.Sprintf("%vsearch?%v", geoLocationBaseUri, params.Encode())
	req, _ := http.NewRequest("GET", uri, nil)

	req.Header.Add("X-RapidAPI-Key", "4bff934c06mshe5799e6b79e4907p1b0ed6jsn9ab038ce5079")
	req.Header.Add("X-RapidAPI-Host", "verify-and-geocode-address.p.rapidapi.com")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	//body, err := ioutil.ReadAll(res.Body)
	//if err != nil {
	//	return nil, err
	//}
	//fmt.Println(res)
	//fmt.Println(string(body))
	return ioutil.ReadAll(res.Body)
}

func address(addressData struct {
	Line1    string `json:"line1"`
	Line2    string `json:"line2"`
	Locality string `json:"locality"`
	Region   string `json:"region"`
	Country  string `json:"country"`
}) (addressSearch string) {

	return fmt.Sprintf(``)
}
