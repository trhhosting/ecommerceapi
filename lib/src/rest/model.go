package rest

type GeoLocationData struct {
	Features []struct {
		Geometry struct {
			Coordinates []float64 `json:"coordinates"`
			Type        string    `json:"type"`
		} `json:"geometry"`
		Properties struct {
			AddressLine1 string `json:"address_line1"`
			AddressLine2 string `json:"address_line2"`
			City         string `json:"city"`
			Country      string `json:"country"`
			CountryCode  string `json:"country_code"`
			County       string `json:"county"`
			Datasource   struct {
				Attribution string `json:"attribution"`
				License     string `json:"license"`
				Sourcename  string `json:"sourcename"`
			} `json:"datasource"`
			District    string  `json:"district"`
			Formatted   string  `json:"formatted"`
			Housenumber string  `json:"housenumber"`
			Lat         float64 `json:"lat"`
			Lon         float64 `json:"lon"`
			PlaceID     string  `json:"place_id"`
			Postcode    string  `json:"postcode"`
			Rank        struct {
				Confidence            int64   `json:"confidence"`
				ConfidenceCityLevel   int64   `json:"confidence_city_level"`
				ConfidenceStreetLevel int64   `json:"confidence_street_level"`
				MatchType             string  `json:"match_type"`
				Popularity            float64 `json:"popularity"`
			} `json:"rank"`
			ResultType string `json:"result_type"`
			State      string `json:"state"`
			StateCode  string `json:"state_code"`
			Street     string `json:"street"`
		} `json:"properties"`
		Type string `json:"type"`
	} `json:"features"`
	Query struct {
		Parsed struct {
			City         string `json:"city"`
			Country      string `json:"country"`
			ExpectedType string `json:"expected_type"`
			Housenumber  string `json:"housenumber"`
			State        string `json:"state"`
			Street       string `json:"street"`
		} `json:"parsed"`
		Text string `json:"text"`
	} `json:"query"`
	Type string `json:"type"`
}
