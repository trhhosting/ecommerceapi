package rest

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"testing"
)

func TestGeoLocation_Get(t *testing.T) {
	var x = GeoLocation{
		Address: struct {
			Line1    string `json:"line1"`
			Locality string `json:"locality"`
			Region   string `json:"region"`
			Country  string `json:"country"`
		}{
			Line1:    "250 Main St",
			Locality: "Hartford",
			Region:   "CT",
			Country:  "USA",
		},
		Limit:  1,
		Lang:   "en",
		Filter: "us",
		Bias:   "proximity:41.2257145,52.971411",
	}
	dat, err := x.Get()
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	err = ioutil.WriteFile("data/resp.json", dat, 0644)
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	var location GeoLocationData
	_ = json.Unmarshal(dat, &location)
	fmt.Println(location.Features[0].Properties.Lat)
	fmt.Println(location.Features[0].Properties.Lon)
}

func TestGeoLocation_Get2(t *testing.T) {
}
