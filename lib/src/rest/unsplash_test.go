package rest

import (
	"fmt"
	"io/ioutil"
	"testing"
)

func TestUnsplash_Get(t *testing.T) {
	x := Unsplash{
		Uri:    "photos/random?page=1&query=Shopping",
		ApiKey: "hnneJaddLEF54Dk2kZ4m1oriJpACllaNiY8W0EEQ4Zw",
	}
	dat, err := x.Get()
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	fmt.Println(string(dat))
	ioutil.WriteFile("data/unsplash.json", dat, 0644)
}
