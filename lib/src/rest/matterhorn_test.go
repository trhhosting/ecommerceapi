package rest

import (
	"fmt"
	"io/ioutil"
	"testing"
)

func TestMatterhorn_Get(t *testing.T) {
	dat, err := Matterhorn{}.Get()
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	err = ioutil.WriteFile("matterhorn_products.xml", dat, 0644)
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
}
