package rest

import (
	"fmt"
	"github.com/go-resty/resty/v2"
)

type Unsplash struct {
	ApiKey  string `json:"api_key"`
	Uri     string `json:"uri"`
	Payload []byte `json:"payload"`
}

const unsplashUri = "https://api.unsplash.com"

/*
AccessKey : hnneJaddLEF54Dk2kZ4m1oriJpACllaNiY8W0EEQ4Zw
SecretKey : CmaIEQVbaekZwPI-6oCPwQAzvVd2LcT9J26ueiFn63M
*/

func (un Unsplash) Get() (rsp []byte, err error) {

	client := resty.New()
	client.SetHeaders(map[string]string{
		"User-Agent":    UserAgent,
		"Content-Type":  "application/json",
		"Authorization": "Client-ID " + un.ApiKey,
	})
	//client.SetAuthToken(un.ApiKey)
	uri := fmt.Sprintf("%v/%v", unsplashUri, un.Uri)
	resp, err := client.R().Get(uri)
	return resp.Body(), err
}
func (un Unsplash) Post() (rsp []byte, err error) {
	client := resty.New()
	client.SetHeaders(map[string]string{
		"User-Agent":    UserAgent,
		"Content-Type":  "application/json",
		"Authorization": "Client-ID " + un.ApiKey,
	})
	//client.SetAuthToken(cj.ApiKey)
	uri := fmt.Sprintf("%v/%v", unsplashUri, un.Uri)
	resp, err := client.R().
		SetBody(un.Payload).
		Post(uri)

	return resp.Body(), err
}
func (un Unsplash) Put() (rsp []byte, err error) {

	client := resty.New()
	client.SetHeaders(map[string]string{
		"User-Agent":    UserAgent,
		"Content-Type":  "application/json",
		"Authorization": "Client-ID " + un.ApiKey,
	})
	//client.SetAuthToken(cj.ApiKey)
	uri := fmt.Sprintf("%v/%v", unsplashUri, un.Uri)
	resp, err := client.R().
		SetBody(un.Payload).
		Post(uri)

	return resp.Body(), err
}
func (un Unsplash) Del() (rsp []byte, err error) {

	client := resty.New()
	client.SetHeaders(map[string]string{
		"User-Agent":    UserAgent,
		"Content-Type":  "application/json",
		"Authorization": "Client-ID " + un.ApiKey,
	})
	//client.SetAuthToken(cj.ApiKey)
	uri := fmt.Sprintf("%v/%v", unsplashUri, un.Uri)
	resp, err := client.R().
		SetBody(un.Payload).
		Post(uri)

	return resp.Body(), err
}
