package rest

import (
	"fmt"
	"io/ioutil"
	"testing"
)

func TestGetCardholders(t *testing.T) {
	var x = GasBuddy{
		Zipcode:   "01011",
		FuelType:  3,
		MaxHours:  4,
		PayMethod: "all",
	}
	dat, err := x.Get()
	if err != nil {
		t.Name()
		fmt.Println(err)
		t.Fail()
	}
	fmt.Println(string(dat))
	ioutil.WriteFile("results.html", dat, 0644)
}
