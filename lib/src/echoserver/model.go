package echoserver

import "ecommerceAPI/lib/src/getTerms"

type Company struct {
	Company    getTerms.Company `json:"company_info"`
	PolicyFile string           `json:"policy_file"`
}
