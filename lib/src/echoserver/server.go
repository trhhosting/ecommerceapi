package echoserver

import (
	"ecommerceAPI/lib/src/cjdropshipping"
	"ecommerceAPI/lib/src/getTerms"
	"encoding/base64"
	"fmt"
	"github.com/Jeffail/gabs/v2"
	"gitlab.com/labstack/echo"
	"math/rand"
	"net"
	"net/http"
	"os"
	"strings"
	"time"
)

func check(ip string) bool {
	trial := net.ParseIP(ip)
	if os.Getenv("nmpw3xEcjogyzbfA4uqr") == "zj9shEHitCdFwvmarygo" {
		fmt.Println(trial)
		fmt.Println(trial.To4())
	}

	if trial.To4() == nil {
		//fmt.Printf("%v is not an IPv4 address\n", trial)
		return false
	}
	return true
}

type TLMSMSServer struct {
	Port      string
	IPAddress string
}

func (t TLMSMSServer) TLMServer() {
	e.GET("/api/v1/products/:category", getCategoryHandler)
	e.POST("/api/v1/policy/", func(c echo.Context) (err error) {
		company := new(Company)
		if err = c.Bind(company); err != nil {
			return c.String(http.StatusInternalServerError, "Error sending policy")
		}
		resp, err := getTerms.PolicyGenerator(company.PolicyFile, company.Company)
		return c.String(200, resp)
	})

	e.POST("/api/v1/p/", nil)
	if t.IPAddress != "" {
		x := check(t.IPAddress)
		if x {
			startStr := fmt.Sprintf("%v:%v", t.IPAddress, t.Port)
			e.Start(startStr)

		} else {
			startStr := fmt.Sprintf("%v:%v", "0.0.0.0", t.Port)
			e.Start(startStr)
		}
	} else {
		startStr := fmt.Sprintf("%v:%v", "localhost", t.Port)
		e.Start(startStr)
	}
}
func getCategoryHandler(e echo.Context) (err error) {
	category := e.Param("category")
	if os.Getenv("PRODCAT") != "" && os.Getenv("PRODCAT") != "default" {
		category = os.Getenv("PRODCAT")
		fmt.Println(category)
	}
	uDec, err := base64.URLEncoding.DecodeString(category)
	if err != nil {
		return e.JSON(300, gabs.Wrap(struct{}{}).Bytes())

	}
	category = string(uDec)
	category = strings.ReplaceAll(category, `"`, "")
	category = strings.ReplaceAll(category, "_", " ")
	category = strings.ReplaceAll(category, "+", "&")
	categoryID, err := cjdropshipping.GetCategoryID(category)
	if err != nil {
		return e.JSON(300, gabs.Wrap(struct{}{}).Bytes())
	}
	//checkCategoryID := gabs.Wrap(categoryID).String()
	//fmt.Println(checkCategoryID)
	if categoryID == nil {
		return e.JSON(200, gabs.Wrap(struct{}{}).Bytes())
	}

	rand.Seed(time.Now().UnixNano())
	catPick := rand.Intn(len(categoryID))
	products, err := cjdropshipping.GetProducts(categoryID[catPick].ID)
	if err != nil {
		return e.JSON(300, gabs.Wrap(struct{}{}).Bytes())
	}
	fmt.Println(fmt.Sprintf("user-agent: %v\n category: %v \nIP Address: %v", e.Request().UserAgent(), categoryID[catPick], e.RealIP()))
	return e.JSONBlob(200, gabs.Wrap(products).Bytes())
}
