package gorm

import (
	"ecommerceAPI/lib/src/configure"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

func loadConfig() (config Cockroach) {
	configure.SetupConfig()
	dat, err := readConfig("tlmconfig.json")
	if err != nil {
		fmt.Println(err)
		return Cockroach{}
	}
	pth, _ := os.Getwd()
	r := os.PathSeparator
	var p string
	if runtime.GOOS == "windows" {
		for _, value := range strings.Split(pth, string(r)) {
			if value == "C:" {
				continue
			}
			if p == "" {
				p = "/" + value
			} else {
				p = p + "/" + value

			}
		}
	} else {
		p = pth
	}
	//fmt.Println(p)

	json.Unmarshal(dat, &config)
	config.SSLCert = p + "/" + config.SSLCert
	config.SSLKey = p + "/" + config.SSLKey
	config.SSLRootCert = p + "/" + config.SSLRootCert

	return
}

func readConfig(filename string) (data []byte, err error) {
	pth := filepath.Join(".config", filename)
	return ioutil.ReadFile(pth)

}
