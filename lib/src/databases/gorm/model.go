package gorm

type Cockroach struct {
	Adapter     string `json:"adapter"`
	Host        string `json:"host"`
	Name        string `json:"name"`
	Password    string `json:"password"`
	Port        string `json:"port"`
	Root        string `json:"root"`
	RootCert    string `json:"rootcert"`
	RootKey     string `json:"rootkey"`
	SSLCert     string `json:"sslcert"`
	SSLKey      string `json:"sslkey"`
	SSLMode     string `json:"sslmode"`
	SSLRootCert string `json:"sslrootcert"`
	User        string `json:"user"`
}