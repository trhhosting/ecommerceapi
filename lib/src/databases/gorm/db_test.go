package gorm

import (
	"testing"
)

func TestCreateDatabaseTables(t *testing.T) {
	DropDb()
	CreateDb()
	err := CreateDatabaseTables()
	if err != nil {
		t.Fail()
	}
}
func TestMigrateDatabaseTables(t *testing.T) {
	err := MigrateDatabaseTables()
	if err != nil {
		t.Fail()
	}
}
func TestGetDataConnection(t *testing.T) {
	GetDataConnection()
}