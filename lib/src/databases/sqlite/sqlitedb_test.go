package sqlite

import (
	"encoding/json"
	"fmt"
	"strings"
	"testing"
)

func TestConnectionSQLite(t *testing.T) {
	var db = ConnectionSQLite("data/data.db")

	defer db.Close()
	//var tbl []model.AnswerChoices

	var dat []struct {
		Id       int    `json:"id"`
		Keywords string `json:"keywords"`
	}

	db.Raw("select id, keywords from keywords").Scan(&dat)

	func(d []struct {
		Id       int    `json:"id"`
		Keywords string `json:"keywords"`
	}) {
		for _, v := range d {
			if v.Keywords == "[]" {
				continue
			}
			var ds []struct {
				Id   int `json:"id"`
				Name string `json:"name"`
			}

			tmp := fmt.Sprintf("%v", v.Keywords)
			tmp = strings.ReplaceAll(tmp, "'", "\"")
			json.Unmarshal([]byte(tmp), &ds)
			for k, v := range ds {
				if k == 0 {
					fmt.Printf("Keyword: %v\n", v.Name)
					continue
				}
				fmt.Printf("count: %v keywords:%v \n",k, v.Name)
			}
			//fmt.Println(v)
		}
	}(dat)
}
