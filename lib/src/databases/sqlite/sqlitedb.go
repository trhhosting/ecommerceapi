package sqlite


import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"log"
)

func ConnectionSQLite(path string) (db *gorm.DB) {
	db, err := gorm.Open("sqlite3", path)
	if err != nil {
		log.Fatal(err)
	}
	//defer db.Close()
	return db
}
