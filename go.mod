module ecommerceAPI

go 1.18

require (
	github.com/Jeffail/gabs/v2 v2.6.1
	github.com/go-resty/resty/v2 v2.7.0
	github.com/jinzhu/gorm v1.9.16
	github.com/lib/pq v1.10.6
	github.com/microcosm-cc/bluemonday v1.0.20
	github.com/rakyll/statik v0.1.7
	github.com/speps/go-hashids v1.0.0
	github.com/spf13/cobra v1.5.0
	github.com/syndtr/goleveldb v1.0.0
	gitlab.com/labstack/echo v3.3.10+incompatible
)

require (
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/gorilla/css v1.0.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/labstack/gommon v0.3.1 // indirect
	github.com/mattn/go-colorable v0.1.11 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-sqlite3 v1.14.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/stretchr/testify v1.8.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.1 // indirect
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd // indirect
	golang.org/x/net v0.0.0-20220826154423-83b083e8dc8b // indirect
	golang.org/x/sys v0.0.0-20220728004956-3c1f35247d10 // indirect
	golang.org/x/text v0.3.7 // indirect
)
